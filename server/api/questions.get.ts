export default defineEventHandler(() => {
  return [
    { category: "HTML", name: "On dit 'un' ou 'une' div ? ", id: 1 },
    { category: "CSS", name: "T'es plutôt flex ou grid ? ", id: 2 },
    { category: "JS", name: "undefined === undefined ? ", id: 3 },
    { category: "HTML", name: "HTML 6, vrai ou hoax ? ", id: 4 },
    {
      category: "CSS",
      name: "Native > Bootstrap > 💩 > Tailwind ? ",
      id: 5,
    },
    { category: "HTML", name: "Dans quel ordre on écrit les H ? ", id: 6 },
    { category: "JS", name: "0 + 0 = 0 ou la tête à toto ? ", id: 7 },
    { category: "HTML", name: "XHTML 1, révolution ou désillusion ? ", id: 8 },
    {
      category: "JS",
      name: "React c'est cool si tu ne bosses pas chez MA ? ",
      id: 9,
    },
    { category: "JS", name: "jQuery: jkéri ou j'y cou ait rit ? ", id: 10 },
    {
      category: "HTML",
      name: "La balise BR est-elle auto-fermante ? ",
      id: 11,
    },
  ];
});
