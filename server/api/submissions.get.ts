export default defineEventHandler(() => {
  return [
    { id: 1, status: "CORRECT" },
    { id: 2, status: "INCORRECT" },
    { id: 3, status: "PARTIALLY_CORRECT" },
    { id: 4, status: "CORRECT" },
    { id: 6, status: "INCORRECT" },
    { id: 7, status: "PARTIALLY_CORRECT" },
    { id: 8, status: "INCORRECT" },
    { id: 10, status: "PARTIALLY_CORRECT" },
    { id: 11, status: "CORRECT" },
  ];
});
